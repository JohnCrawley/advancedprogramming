/*
Name: John F.
Student ID: x19141301
Created On: 20/10/2021
Last Modified On: 04/11/2021
Description: 
------------------------
This is the TCP server application that
will host and save the events or remove them
*/
//Package//
package dublineventscalenderserver;
//Import list//
import java.io.*;                           // Import all IO packages//
import java.net.*;                          // Import all NET packages//
import java.util.ArrayList;                 // Import ArrayList package//


//TCP Main Class//
public class DublinEventsCalenderTCPServer extends Thread
{
    /*
    Private variables list:
    Declaring all private variables at top to be used below
    */
    //Networking variables//
    private static int PORT = 5555;                                                     // Server Port//
    private static ServerSocket serverSocket;                                           // Server Socket//
    //Socket//
    private static Socket socket;                                                       // server: the TCP's server socket //
    //BufferedReaders//
    private static BufferedReader inputBuffered;                                        // Input: read the input//
    //PrintWriter//
    private static PrintWriter output;                                                  // PrintWritter: return an output//
    //I/O Streams//
    private static InputStream inputStream;                                             // Input stream//
    private static DataInputStream dataInputStream;                                     // Data input stream//
    private static DataOutputStream dataOutputStream;                                   // Data output stream//
    private static ObjectInputStream objectInputStream;                                 //
    //Boolean//
    private static boolean isServerRunning = true;                                      // If TRUE = server is on | FALSE = server is off//
    //Strings//
    private static String line = null;                                                  //
    private static String holder = null;                                                //
    private static String cli;                                                          // Input from the client side//
    //ArrayList//
    private static ArrayList<String> serverStoredEvents = new ArrayList<String>();     // ArrayList: Store events for server-side//
        
    
    /*
    Constructor to try start a server port
    */
    public DublinEventsCalenderTCPServer()
    {
        try
        {
            //Set the server socket as the port number//
            serverSocket = new ServerSocket(PORT);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    //Main Method//
    public static void main(String[] args)  
    {        
      //  System.out.println(getEventCalender());
        
        
        //OUTPUT server header text//
        System.out.println("> ========================================= < ");
        System.out.println("> \t  Dublin Event TCP Server  \t    <");
        System.out.println("> ========================================= < ");
        try 
        {
            //Output to the user that the server is waiting on the client//
            System.out.println("> Waiting for client on PORT: " + PORT + " <");
            //Set up the server socket as port value
            serverSocket = new ServerSocket(PORT);
            //Accept the server socket//
            socket = serverSocket.accept();
            //Output to the user that the server connection has been made//
            System.out.println("> Connected to remote client: " + socket.getRemoteSocketAddress() + "! <");
            //Create an input stream attached to the socket server//
            dataInputStream = new DataInputStream(socket.getInputStream());
            //Create output stream attached to the server socket//
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
                
           /*
            while the server boolean is true, the block will loop
            when the user enters, "close" it will end the connection
            */
            while (isServerRunning = true) 
            {

                //Read input from server socket//
                cli = dataInputStream.readUTF();
                cli.toLowerCase();
                /*
                    IF the user enters any of the following commands below, 
                    it will execute them
                 */
                //Command#1:Add date to the array list//
                if (cli.startsWith("add"))
                {
                    addEvent(cli);
                    dataOutputStream.writeUTF("> Date added succesfully! <");
                }            
                //Command#2: Remove from array list if it exists//
                else if (cli.startsWith("remove"))
                {
                    removeFromStoredEvents(cli);  
                     dataOutputStream.writeUTF("> Date removed succesfully! <");
                }
                //Command#3: view Array list //
                else if (cli.startsWith("view"))
                {
                   if(!serverStoredEvents.isEmpty())
                   {
                     //Print arraylist events to the stream//
                      dataOutputStream.writeUTF(getEventCalender());
                   }
                   else
                   {
                     //Print that the calender is empty//
                      dataOutputStream.writeUTF("> Event calender is empty. <\n> Please enter events < ");
                   }
                } //Command#4: help - list commands//
                else if (cli.startsWith("commands") || cli.startsWith("help")) 
                {
                    System.out.println("> User asked for the list of commands <");
                } 
                //Close all connections//
                else if (cli.equalsIgnoreCase("close")) 
                {
                    System.out.println("> closing connections <");
                    socket.close();
                    serverSocket.close();
                    isServerRunning = false;
                }
 
            }
    
        } 
        catch (IOException e) 
        {
            System.out.println(e);
        }  
        finally
        {
            stopConnection();
        }
    } 
   
    /*
    METHODS
    reusable code of methods are here
    */
     
    /*
    METHOD: StopConnection:
    stop all connections in the server
    */
    public static void stopConnection() 
    {
        try
        {
        socket.close();
        inputStream.close();
        dataOutputStream.close();
        }
        catch(Exception closingError)
        {
            System.out.println("> Error: " + closingError + " <");
        }
    }
 
    /*
    METHOD: addEvent:
    Add a date to the ArrayList stored on the server//
    */
    public static void addEvent(String eventDate)
    {
        String removedPrefix = eventDate.replaceFirst("add", "").toLowerCase();
        serverStoredEvents.add(removedPrefix);
        
    }
    /*
    METHOD: getEventCalender
    return all the events on the calender
    */
    public static String getEventCalender()
    {
        String result = "> Events: <\n";
        
        if (serverStoredEvents.isEmpty())
        {
            try
            {
               dataOutputStream.writeUTF("> Event list is empty <"); 
            }
            catch(IOException io)
            {
                io.printStackTrace();
            }
            
        }
        else
        {
            for(int i = 0; i < serverStoredEvents.size(); i++)
            {
                result += "> " + serverStoredEvents.get(i) + " <\n";
            }       
        }
      
        //Return result of the array list
        return result;
    }
   /*
    METHOD: RemoveFromStoredEvents:
    remove the item described by user from the 
    event list
    */
    public static void removeFromStoredEvents(String removeEventDate) throws IOException
    {
       String removedPrefix = removeEventDate.replaceFirst("remove", "").toLowerCase();
      
       
        //If serverstoredevents contains date - remove//
       if(serverStoredEvents.contains(removedPrefix))
       {
          
           serverStoredEvents.remove(removedPrefix);
       }
       else
       {
           System.out.println("NOT REMOVED FROM ARRAYLIST");
       }

       
       
    }
    
    
    
    
    
    
}




   