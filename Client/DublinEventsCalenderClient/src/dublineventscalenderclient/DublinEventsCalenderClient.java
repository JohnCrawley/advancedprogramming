/*
Name: John F.
Student ID: x19141301
Created On: 20/10/2021
Last Modified On: 04/11/2021
Description: 
------------------------
Main file for client app'.
This is the application the user will 
load up to add/remove/view the arraylist on the server
*/

//Package//
package dublineventscalenderclient;
//Import list//
import java.io.*;                           // Import all IO packages//
import java.net.*;                          // Import all NET packages//
import java.util.ArrayList;                 //Import ArrayList package//

//Client main Class//
public class DublinEventsCalenderClient 
{
    /*
    Private variables list:
    Declaring all private variables at top to be used below
     */
    //Networking variables//
    private static String ADDRESS = "127.0.0.1";            // Localhost server address //
    private static int PORT       = 5555;                   // Port to the server // 
    private static InetAddress inetAddress;                 // the iNet Address //
    private static ServerSocket serverSocket;               // Server Socket//
    private static Socket socket;                           // sever socket//
    private static OutputStream outputToServer;             // Ouput to server stream// 
   
    private static ObjectOutputStream objectOutputStream;   // Object Output Stream//
    private static DataOutputStream outputStream;           // Data output stream//
    private static InputStream inFromServer;                // Input from server stream//
    private static DataInputStream inputFromServer;         // Data input stream//
    //Buffered Reader//
    private static BufferedReader userInput;                //User Input Reader//
    //PrintWriter//
    private static PrintWriter printWriter;                 // PrintWritter: return an output//
    //ArrayList//
    ArrayList<String> events = new ArrayList<String>();     //Store event dates in the array list//
    //BOOL//
    private static boolean isServerIsRunning = false;       // While TRUE = server is running ||||| WHILE FALSE = server is NOT running//
    //String//
    private static String outputFromStream = "";
    private static String tempInput = "";                   //Set a temp string for input values//
    //Main Method//
    public static void main(String[] args) throws IOException
    {
        //OUTPUT client header text//
        System.out.println("> ========================================= < ");
        System.out.println("> \t  Dublin Event Client Server  \t    <");
        System.out.println("> ========================================= < ");
        //Output to the user that there's an attempt to connect to the TCP server//
        System.out.println("> Attempting to connect to TCP server... <");
        
        /*
        TRY: 
        connect to the server-side of the application
        */
        try 
        {
            //Assign the inet and socket values//
            inetAddress =  InetAddress.getByName(ADDRESS);
            socket = new Socket(inetAddress, PORT);
            //Output to the user that the connection has been made//
            System.out.println("> Connected to remote client: " + socket.getRemoteSocketAddress());
            //Set server running = true//
            isServerIsRunning = true;
            
            
           /*
            while the server boolean is true, the block will loop
            when the user enters, "close" it will end the connection
            */
            while(isServerIsRunning = true)
            {
              
                if(tempInput.equalsIgnoreCase("close"))
                {
                    System.out.println("> Server is closing... <");
                    socket.close();
                    isServerIsRunning = false;
                    break;
                }    
                else
                {
                    //Set the bufferedreader [userInput] to system input//
                    userInput = new BufferedReader(new InputStreamReader(System.in));
                    //Create output stream to be attached to the socket//
                    outputStream = new DataOutputStream(socket.getOutputStream());
                    System.out.println("> Enter a command: ");
                    //Create input stream to socket//
                    inputFromServer = new DataInputStream(socket.getInputStream());
                    //Set the temp input string to the read line cli//      
                    tempInput = userInput.readLine();
                    //Write the command to the output stream//
                    outputStream.writeUTF(tempInput);
                    //Flush the content of the buffer to the output stream//
                    outputStream.flush();
                    
                    /*
                    IF the user enters any of the following commands below, 
                    it will execute them
                    */
                    
                    //Command#1:Add to array list
                    if (tempInput.startsWith("add")) 
                    {
                        System.out.println("> Attempting to add... <");
                        outputStream.writeUTF(tempInput);
                        System.out.println(inputFromServer);
                        
                    } //Command#2: Remove from array list
                    else if (tempInput.startsWith("remove")) 
                    {
                        
                        System.out.println("> Attemping to remove the following: " + tempInput.replaceFirst("remove", "") + " <");
                        outputStream.writeUTF(tempInput);
                        
                    } //Command#3: view Array list
                    else if (tempInput.startsWith("view")) 
                    {
                        outputFromStream = inputFromServer.readUTF();
                        System.out.println(outputFromStream);
                    }
                   //Command#4: help
                    else if (tempInput.startsWith("commands") || tempInput.startsWith("help")) 
                    {
                        System.out.println("> List of Commands: < \n"
                                + "> 1. add (date) + (details) = add event to the calender <\n"
                                + "> 2. remove (date) = remove event from calender <\n"
                                + "> 3. view = view the entire calender list <\n"
                                + "> 4. commands/help = list of all commands <\n");
                    }
                    
                    
             
                }

            
            }

            
            
        }
        //Catch and output error//
        catch(IOException exception)
        {
            System.out.println("-------------------------------------");
            System.out.println(">\tError: " + exception + "\t<");
            System.out.println("\n> Possible Solutions: "
                    + "<\n-------------------------------------\n"
                    + "> 1. Run the TCP Server first <"
                    + "\n"
                    + "> 2. Check the iNet address & port is correct <");
             System.out.println("-------------------------------------");
        }
        
    }
    
    
    

    
    

}
